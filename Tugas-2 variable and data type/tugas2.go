package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {
	// soal 1
	variable1 := "Bootcamp"
	variable2 := "Digital"
	variable3 := "Skill"
	variable4 := "Sanbercode"
	variable5 := "Golang"

	fmt.Println(variable1+" "+variable2+" "+variable3+" "+variable4+" "+variable5)

	// soal 2
	halo := "Halo Dunia"
	fmt.Println(strings.Replace(halo, "Dunia", "Golang", -1))

	// soal 3
	var kataPertama = "saya";
	var kataKedua = "senang";
	var kataKetiga = "belajar";
	var kataKeempat = "golang";
	fmt.Println(kataPertama+" "+strings.Title(kataKedua)+" "+strings.Replace(kataKetiga, "r", "R", 1)+" "+strings.ToUpper(kataKeempat))

	// soal 4
	var angkaPertama= "8";
	var numPertama, _ = strconv.Atoi(angkaPertama)
	var angkaKedua= "5";
	var numKedua, _ = strconv.Atoi(angkaKedua)
	var angkaKetiga= "6";
	var numKetiga, _ = strconv.Atoi(angkaKetiga)
	var angkaKeempat = "7";
	var numKeempat, _ = strconv.Atoi(angkaKeempat)

	fmt.Println(numPertama+numKedua+numKetiga+numKeempat)

	// soal 5
	kalimat := "halo halo bandung"
	angka := 2021
	fmt.Println(`"`+strings.Replace(kalimat, "halo", "Hi", -1)+`" - `, angka)

}