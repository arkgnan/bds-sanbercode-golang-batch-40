package main

import (
	"fmt"
	"net/http"
	"web-server/helpers"
)

func greet(w http.ResponseWriter, r *http.Request) {
	jarijari := 7
	tinggiTabung := 10

	var volume float64 = helpers.Volume(float64(jarijari), float64(tinggiTabung))
	var luasAlas float64 = helpers.LuasAlas(float64(jarijari))
	var kelilingAlas float64 = helpers.KelilingAlas(float64(jarijari))


	fmt.Fprintf(w, "jariJari : %d, tinggi: %d, volume : %.3f, luas alas: %.3f, keliling alas: %.3f", jarijari, tinggiTabung, volume, luasAlas, kelilingAlas)
}

func main() {
	http.HandleFunc("/", greet)
	http.ListenAndServe(":8080", nil)
}