package helpers

import (
	"math"
)

func Volume(jarijari, tinggi float64) float64 {
	return math.Pi * math.Pow(jarijari, 2) * tinggi
}

func LuasAlas(jarijari float64) float64 {
	return math.Pi * math.Pow(jarijari, 2)
}

func KelilingAlas(jarijari float64) float64 {
	return 2 * math.Pi * jarijari
}