package main

import (
	"fmt"
	"strings"
)

func main(){
	// soal 1
	panjang := 12
	lebar := 4
	tinggi := 8
	
	luas := luasPersegiPanjang(panjang, lebar)
	keliling := kelilingPersegiPanjang(panjang, lebar)
	volume := volumeBalok(panjang, lebar, tinggi)

	fmt.Println(luas) 
	fmt.Println(keliling)
	fmt.Println(volume)

	// soal 2
	john := introduce("John", "laki-laki", "penulis", "30")
	fmt.Println(john) // Menampilkan "Pak John adalah seorang penulis yang berusia 30 tahun"

	sarah := introduce("Sarah", "perempuan", "model", "28")
	fmt.Println(sarah) // Menampilkan "Bu Sarah adalah seorang model yang berusia 28 tahun"

	// soal 3
	var buah = []string{"semangka", "jeruk", "melon", "pepaya"}

	var buahFavoritJohn = buahFavorit("John", buah...)

	fmt.Println(buahFavoritJohn)
	// halo nama saya john dan buah favorit saya adalah "semangka", "jeruk", "melon", "pepaya"

	// soale 4
	var dataFilm = []map[string]string{}
	var tambahDataFilm = func(title, duration, genre, releaseYear string){
		var film = map[string]string{
			"genre": genre,
			"jam": duration,
			"tahun": releaseYear,
			"title": title,
		}
		dataFilm = append(dataFilm, film)
	}

	tambahDataFilm("LOTR", "2 jam", "action", "1999")
	tambahDataFilm("avenger", "2 jam", "action", "2019")
	tambahDataFilm("spiderman", "2 jam", "action", "2004")
	tambahDataFilm("juon", "2 jam", "horror", "2004")

	for _, item := range dataFilm {
		fmt.Println(item)
	}

}

func luasPersegiPanjang(p int, l int) int{
	return p*l
}

func kelilingPersegiPanjang(p int, l int) int{
	return 2*(p+l)
}

func volumeBalok(p int, l int, t int) int{
	return p*l*t
}

func introduce(name, gender, job, age string)(title string){
	title = name+" "+gender+" "+job+" "+age
	return title
}

func buahFavorit(name string, fruits ...string) string {
	var buahString string = strings.Join(fruits, "\", \"");
	return "halo nama saya "+name+" dan buah favorit saya adalah \""+buahString+"\""

}