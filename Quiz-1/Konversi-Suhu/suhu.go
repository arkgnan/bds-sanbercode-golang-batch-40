package main

import (
	"fmt"
)

func main() {
	var suhu int = 10

	suhuFarenheit := convertFarenheit(suhu)

	cetak(suhuFarenheit)
}

func convertFarenheit(celcius int) int {
	return (9/5)*celcius+32
}

func cetak(suhu int) {
	fmt.Println(suhu)
}