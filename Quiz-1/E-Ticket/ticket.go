package main

import "fmt"

func main() {
	var tiket = map[string]string{
		"pemilik": "Gus Samsudin",
		"kereta": "Mataram",
		"perjalanan": "Jakarta - Surabaya",
	}

	var tiketLengkap = setKodeTiket("H1D1C9100", tiket)
	cetakTiket(tiketLengkap)
	var tiketBaru = ubahNamaPemilik("Doni Riyadi", tiket)
	cetakTiket(tiketBaru)

}

func setKodeTiket(KodeTiket string, tiket map[string]string) map[string]string {
	tiket["kode_tiket"] = KodeTiket
	return tiket	
}

func cetakTiket(tiket map[string]string) {
	fmt.Println("E-Ticket")
	for key, value := range tiket {
		fmt.Println(key, ":", value)
	}
	fmt.Println("-----------")
}

func ubahNamaPemilik(nama string, tiket map[string]string) (tiketBaru map[string]string) {
	tiketBaru = tiket
	tiketBaru["pemilik"] = nama
	return tiketBaru
}
