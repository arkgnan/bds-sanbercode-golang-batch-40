package main

import (
	"errors"
	"fmt"
	"sort"
	"strconv"
	"time"
)


func logging(kalimat string, tahun int){
	fmt.Println(kalimat, tahun)
}

func kelilingSegitigaSamaSisi(angka int, status bool)interface{}{
	if status {
		if angka==0 {
			return errors.New("Maaf anda belum menginput sisi dari segitiga sama sisi")
		} else {
			sisi := strconv.Itoa(angka)
			keliling := strconv.Itoa(angka+angka+angka)
			response := "keliling segitiga sama sisinya dengan sisi "+sisi+" cm adalah "+keliling+" cm"
			return response
		}
	} else {
		if angka==0 {
			panic(errors.New("Maaf anda belum menginput sisi dari segitiga sama sisi"))
		} else {
			return angka
		}
	}

}

func catch() {
    if r := recover(); r != nil {
        fmt.Println("Error :", r)
    } else {
        fmt.Println("Application running perfectly")
    }
}

func tambahAngka(penambah int, angka *int){
	*angka = *angka + penambah
}

func cetakAngka(angka *int){
	fmt.Println(*angka)
}

func tambahHape(brands []string, phones *[]string){
	*phones = append(*phones, brands...)
}
func urutkan(list *[]string){
	sort.Strings(*list)
	for i, urut := range *list{
		time.Sleep(time.Second)
		fmt.Println(i+1, urut)
	}
}

func main(){
	// soal 3
	angka := 1

	defer cetakAngka(&angka)

	tambahAngka(7, &angka)

	tambahAngka(6, &angka)

	tambahAngka(-1, &angka)

	tambahAngka(9, &angka)
	
	// soal 4
	var phones = []string{}
	tambahHape([]string{"Xiaomi", "Asus", "Iphone", "Samsung", "Oppo", "Realme", "Vivo"}, &phones)
	urutkan(&phones)

	// soal 1
	defer logging("Golang Backend Development", 2021)
	fmt.Println("Run application")

	// soal 2
	defer catch()
	fmt.Println(kelilingSegitigaSamaSisi(4, true))

	fmt.Println(kelilingSegitigaSamaSisi(8, false))

	fmt.Println(kelilingSegitigaSamaSisi(0, true))

	fmt.Println(kelilingSegitigaSamaSisi(0, false))

}