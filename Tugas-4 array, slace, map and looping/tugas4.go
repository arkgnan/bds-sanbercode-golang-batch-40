package main

import (
	"fmt"
)

func main()  {
	// soal 1
	for i := 1; i < 21; i++ {
		if i % 2 == 0 {
			fmt.Println(i, "- Berkualitas")
		} else {
			if i % 3 == 0 {
				fmt.Println(i, "- I Love Coding ")
			} else {
				fmt.Println(i, "- Santai")
			}
		}
	}

	// soal 2
	var alas = 1
	for alas < 8 {
		for i := 0; i < alas; i++ {
			fmt.Print("#")
		}
		fmt.Print("\n")
		alas++
	}

	// soal 3
	var kalimat = [...]string{"aku", "dan", "saya", "sangat", "senang", "belajar", "golang"}
	fmt.Println(kalimat[2:])

	// soal 4
	var sayuran = []string{}
	var extraSayur = []string{"Bayam", "Buncis", "Kangkung", "Kubis", "Seledri", "Tauge", "Timun"}
	var sayurKomplit = append(sayuran, extraSayur...)
	for i, vegetable := range sayurKomplit {
		fmt.Printf("%d. %s\n", i+1, vegetable)
	}

	// soal 5
	var satuan = map[string]int{
		"panjang": 7,
		"lebar":   4,
		"tinggi":  6,
	}
	var volumeBalok int = 1
	for key, value := range satuan {
		fmt.Println(key, "=", value)
		volumeBalok = volumeBalok * value
	}
	fmt.Printf("Volume Balok = %d", volumeBalok)
}