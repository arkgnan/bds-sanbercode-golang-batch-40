PGDMP     $            
        z         	   db-go-sql    15.1    15.1                0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false                       0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false                       0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false                       1262    16398 	   db-go-sql    DATABASE     �   CREATE DATABASE "db-go-sql" WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE_PROVIDER = libc LOCALE = 'English_United States.1252';
    DROP DATABASE "db-go-sql";
                postgres    false            �            1259    16418    book    TABLE     �  CREATE TABLE public.book (
    id integer NOT NULL,
    title text NOT NULL,
    description text,
    image_url text,
    release_year integer NOT NULL,
    price integer NOT NULL,
    total_page integer NOT NULL,
    thickness character varying(50) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone,
    category_id integer
);
    DROP TABLE public.book;
       public         heap    postgres    false            �            1259    16417    book_id_seq    SEQUENCE     �   CREATE SEQUENCE public.book_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.book_id_seq;
       public          postgres    false    219                       0    0    book_id_seq    SEQUENCE OWNED BY     ;   ALTER SEQUENCE public.book_id_seq OWNED BY public.book.id;
          public          postgres    false    218            �            1259    16411    category    TABLE     �   CREATE TABLE public.category (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone
);
    DROP TABLE public.category;
       public         heap    postgres    false            �            1259    16410    category_id_seq    SEQUENCE     �   CREATE SEQUENCE public.category_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.category_id_seq;
       public          postgres    false    217                       0    0    category_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.category_id_seq OWNED BY public.category.id;
          public          postgres    false    216            �            1259    16400 	   employees    TABLE     �   CREATE TABLE public.employees (
    id integer NOT NULL,
    full_name character varying(50) NOT NULL,
    email text NOT NULL,
    age integer NOT NULL,
    division character varying(20) NOT NULL
);
    DROP TABLE public.employees;
       public         heap    postgres    false            �            1259    16399    employees_id_seq    SEQUENCE     �   CREATE SEQUENCE public.employees_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.employees_id_seq;
       public          postgres    false    215                       0    0    employees_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.employees_id_seq OWNED BY public.employees.id;
          public          postgres    false    214            q           2604    16421    book id    DEFAULT     b   ALTER TABLE ONLY public.book ALTER COLUMN id SET DEFAULT nextval('public.book_id_seq'::regclass);
 6   ALTER TABLE public.book ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    218    219    219            p           2604    16414    category id    DEFAULT     j   ALTER TABLE ONLY public.category ALTER COLUMN id SET DEFAULT nextval('public.category_id_seq'::regclass);
 :   ALTER TABLE public.category ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    217    216    217            o           2604    16403    employees id    DEFAULT     l   ALTER TABLE ONLY public.employees ALTER COLUMN id SET DEFAULT nextval('public.employees_id_seq'::regclass);
 ;   ALTER TABLE public.employees ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    214    215    215                      0    16418    book 
   TABLE DATA           �   COPY public.book (id, title, description, image_url, release_year, price, total_page, thickness, created_at, updated_at, category_id) FROM stdin;
    public          postgres    false    219   �                 0    16411    category 
   TABLE DATA           D   COPY public.category (id, name, created_at, updated_at) FROM stdin;
    public          postgres    false    217   �       
          0    16400 	   employees 
   TABLE DATA           H   COPY public.employees (id, full_name, email, age, division) FROM stdin;
    public          postgres    false    215   J                  0    0    book_id_seq    SEQUENCE SET     9   SELECT pg_catalog.setval('public.book_id_seq', 1, true);
          public          postgres    false    218                       0    0    category_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.category_id_seq', 2, true);
          public          postgres    false    216                       0    0    employees_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.employees_id_seq', 1, true);
          public          postgres    false    214            y           2606    16425    book book_pkey 
   CONSTRAINT     L   ALTER TABLE ONLY public.book
    ADD CONSTRAINT book_pkey PRIMARY KEY (id);
 8   ALTER TABLE ONLY public.book DROP CONSTRAINT book_pkey;
       public            postgres    false    219            w           2606    16416    category category_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.category
    ADD CONSTRAINT category_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.category DROP CONSTRAINT category_pkey;
       public            postgres    false    217            s           2606    16409    employees employees_email_key 
   CONSTRAINT     Y   ALTER TABLE ONLY public.employees
    ADD CONSTRAINT employees_email_key UNIQUE (email);
 G   ALTER TABLE ONLY public.employees DROP CONSTRAINT employees_email_key;
       public            postgres    false    215            u           2606    16407    employees employees_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.employees
    ADD CONSTRAINT employees_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.employees DROP CONSTRAINT employees_pkey;
       public            postgres    false    215            z           2606    16426    book fk_book_category    FK CONSTRAINT     {   ALTER TABLE ONLY public.book
    ADD CONSTRAINT fk_book_category FOREIGN KEY (category_id) REFERENCES public.category(id);
 ?   ALTER TABLE ONLY public.book DROP CONSTRAINT fk_book_category;
       public          postgres    false    3191    219    217               _   x���� E���
�agV�F�`�~n��Z�Z$t�z�r� ���bL�>���e��ng}�c�ݗ�2��E��öªM+�bbq         C   x�3��N-N�H,I��4202�5"+0���2���/K�A���20�20B36�22����� ���      
      x������ � �     