package models

import (
	"time"
)

type Category struct{
	Id int `json:"id"`
	Name string `json:"name"`
	CreatedAt, UpdatedAt time.Time
}
