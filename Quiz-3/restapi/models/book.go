package models

import (
	"time"
)

type Book struct{
	Id int `db:"id" json:"id"`
	Title string `db:"title" json:"title"`
	Description string `db:"description" json:"description"`
	ImageUrl string `db:"image_url" json:"image_url"`
	ReleaseYear int `db:"release_year" json:"release_year"`
	Price int `db:"price" json:"price"`
	TotalPage int `db:"total_page" json:"total_page"`
	Thickness string `db:"thickness"`
	CategoryId int `db:"category_id" json:"category_id"`
	CreatedAt time.Time `db:"created_at"`
	UpdatedAt time.Time `db:"updated_at"`
}