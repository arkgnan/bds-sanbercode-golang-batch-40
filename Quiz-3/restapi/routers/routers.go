package routers

import (
	"net/http"
	bangunDatarController "restapi/controllers/bangun_datar"
	bookController "restapi/controllers/book"
	categoryController "restapi/controllers/category"

	"github.com/gin-gonic/gin"
)

var secrets = gin.H{
    "admin":    gin.H{"username": "admin", "password": "password"},
    "editor":    gin.H{"username": "editor", "password": "secret"},
}


func StartServer() *gin.Engine {
	router := gin.Default()

	bangunDatar := router.Group("/bangun-datar")
	{
			
		bangunDatar.GET("/segitiga-sama-sisi", bangunDatarController.SegitigaSamaSisi)
		bangunDatar.GET("/persegi", bangunDatarController.Persegi)
		bangunDatar.GET("/persegi-panjang", bangunDatarController.PersegiPanjang)
		bangunDatar.GET("/lingkaran", bangunDatarController.Lingkaran)
		bangunDatar.GET("/jajar-genjang", bangunDatarController.JajarGenjang)
	}

	category := router.Group("/categories")
	{
		category.GET("/:id/books", categoryController.GetBookByCategory)
		category.GET("/:id", categoryController.GetCategory)
		category.GET("/", categoryController.GetAllCategory)
		category.POST("/", gin.BasicAuth(gin.Accounts{
			"admin": "password",
			"editor": "secret",
		}), categoryController.CreateCategory)
		category.PUT("/:id", gin.BasicAuth(gin.Accounts{
			"admin": "password",
			"editor": "secret",
		}), categoryController.UpdateCategory)
		category.DELETE("/:id", gin.BasicAuth(gin.Accounts{
			"admin": "password",
			"editor": "secret",
		}), categoryController.DeleteCategory)
	}

	books := router.Group("/books")
	{
		books.GET("/:id", bookController.GetBook)
		books.GET("/", bookController.GetAllBook)
		books.POST("/", gin.BasicAuth(gin.Accounts{
			"admin": "password",
			"editor": "secret",
		}), bookController.CreateBook)
		books.PUT("/:id", gin.BasicAuth(gin.Accounts{
			"admin": "password",
			"editor": "secret",
		}), bookController.UpdateBook)
		books.DELETE("/:id", gin.BasicAuth(gin.Accounts{
			"admin": "password",
			"editor": "secret",
		}), bookController.DeleteBook)
	}

	return router
}

func ParamHitungMiddleware(c *gin.Context) {
	// validasi parameter hitung
	hitung, ok := c.GetQuery("hitung");
	if ok {
		if hitung == "" {
			c.Header("Content-Type", "application/json")
			c.JSON(http.StatusNotFound,
				gin.H{"error: ": "Hitung rumus tidak boleh kosong!"})
			c.Abort()
			return
		}
	} else {
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusNotFound,
			gin.H{"error: ": "Invalid hitung parameter!"})
		c.Abort()
		return
	}

	if hitung != "luas" && hitung != "keliling" {
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusNotFound,
			gin.H{"error: ": "Invalid hitung rumus!"})
		c.Abort()
		return
	}

   	c.Next()
}