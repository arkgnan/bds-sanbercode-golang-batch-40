package category

import (
	"database/sql"
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"

	conn "restapi/dbsetup"
	"restapi/models"
)

// declare global variable
var db, err = conn.InitPostgres() 
var t = time.Now()
var formattedDateNow = fmt.Sprintf("%d-%02d-%02d %d:%02d:%02d", t.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), t.Second())
var category models.Category
var categories []models.Category

func CreateCategory(ctx *gin.Context) {	
	if err := ctx.ShouldBindJSON(&category); err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}  

	sqlStatement := `
	INSERT INTO category (name, created_at, updated_at)
	VALUES ($1, $2, $3)
	Returning *
	`
	err = db.QueryRow(sqlStatement, category.Name, formattedDateNow, formattedDateNow).
		Scan(&category.Id, &category.Name, &category.CreatedAt, &category.UpdatedAt)
	if err != nil {
		panic(err)
	}
	ctx.JSON(http.StatusCreated, gin.H {
		"data": category,
	})
}

func UpdateCategory(ctx *gin.Context) {
	categoryId := ctx.Param("id")

	if err := ctx.ShouldBindJSON(&category); err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	sqlStatement := `
	UPDATE category
	SET name = $2, updated_at = $3
	WHERE id = $1;
	`
	res, err := db.Exec(sqlStatement, categoryId, category.Name, formattedDateNow)
	if err != nil {
		panic(err)
	}
	count, err := res.RowsAffected()
	if err != nil {
		panic(err)
	}

	if count==0 {
		ctx.AbortWithStatusJSON(http.StatusNotFound, gin.H{
			"error_status": "Data Not Found",
			"error_message": fmt.Sprintf("Category with id %v not found", categoryId),
		})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"message": fmt.Sprintf("Category with id %v has been successfully updated", categoryId),
	})
}

func GetCategory(ctx *gin.Context) {
	categoryId := ctx.Param("id")

	sqlStatement := `SELECT * from category WHERE id = $1`

	row := db.QueryRow(sqlStatement, categoryId)

	switch err := row.Scan(&category.Id, &category.Name, &category.CreatedAt, &category.UpdatedAt); err {
		case sql.ErrNoRows:
			ctx.AbortWithStatusJSON(http.StatusNotFound, gin.H{
				"error_status": "Data Not Found",
				"error_message": fmt.Sprintf("Category with id %v not found", categoryId),
			})
			return
		case nil:
			ctx.JSON(http.StatusOK, gin.H{
				"data": category,
			})
		default:
			panic(err)
	}
}

func DeleteCategory(ctx *gin.Context) {
	categoryId := ctx.Param("id")

	if err := ctx.ShouldBindJSON(&category); err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	sqlStatement := `
	DELETE from category
	WHERE id = $1;
	`
	res, err := db.Exec(sqlStatement, categoryId)
	if err != nil {
		panic(err)
	}
	count, err := res.RowsAffected()
	if err != nil {
		panic(err)
	}

	if count==0 {
		ctx.AbortWithStatusJSON(http.StatusNotFound, gin.H{
			"error_status": "Data Not Found",
			"error_message": fmt.Sprintf("Category with id %v not found", categoryId),
		})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"message": fmt.Sprintf("Category with id %v has been successfully deleted", categoryId),
	})
}

func GetAllCategory(ctx *gin.Context) {
	sqlStatement := `SELECT * from category`

	rows, err := db.Query(sqlStatement)
	if err != nil {
		panic(err)
	}

	for rows.Next() {
		err = rows.Scan(&category.Id, &category.Name, &category.CreatedAt, &category.UpdatedAt)

		if err != nil {
			panic(err)
		}

		categories = append(categories, category)
	}
	ctx.JSON(http.StatusOK, gin.H{
		"data": categories,
	})
}

func GetBookByCategory(ctx *gin.Context) {
	var book models.Book
	var books []models.Book

	categoryId := ctx.Param("id")

	sqlStatement := `SELECT * from book WHERE category_id = $1`

	rows, err := db.Query(sqlStatement, categoryId)
	if err != nil {
        panic(err)
    }
	defer db.Close()
	for rows.Next() {
		err := rows.Scan(&book.Id, &book.Title, &book.Description, &book.ImageUrl, &book.ReleaseYear, &book.Price, &book.TotalPage, &book.Thickness, &book.CreatedAt, &book.UpdatedAt, &book.CategoryId)
		if err != nil {
			panic(err)
		}
		books = append(books, book)
	}
	ctx.JSON(http.StatusOK, gin.H{
		"data": books,
	})
}
