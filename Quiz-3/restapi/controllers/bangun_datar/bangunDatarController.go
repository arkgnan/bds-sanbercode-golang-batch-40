package bangun_datar

import (
	"math"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

func SegitigaSamaSisi(c *gin.Context) {
	hitung := c.Query("hitung")
	alas := c.Query("alas")
	tinggi := c.Query("tinggi")

	var luasSegitigaSamaSisi = func(a, t float64) float64{
		return 0.5 * a * t
	}

	var kelilingSegitigaSamaSisi = func(a int) int{
		return a + a + a
	}

	if hitung == "luas" {
		alasFloat, _ := strconv.ParseFloat(alas, 64)
		tinggiFloat, _ := strconv.ParseFloat(tinggi, 64)
		getLuas := luasSegitigaSamaSisi(alasFloat, tinggiFloat)
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusOK,
			gin.H{"luas: ": getLuas})
		c.Abort()
		return
	} else {
		alasInt, _ := strconv.Atoi(alas)
		getKeliling := kelilingSegitigaSamaSisi(alasInt)
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusOK,
			gin.H{"keliling: ": getKeliling})
		c.Abort()
		return
	}
}

func Persegi(c *gin.Context) {
	hitung := c.Query("hitung")
	sisi := c.Query("sisi")
	sisiInt, _ := strconv.Atoi(sisi)
	var luasPersegi = func(s int) int{
		return s * s
	}

	var kelilingPersegi = func(s int) int{
		return 4 * s
	}

	if hitung == "luas" {
		getLuas := luasPersegi(sisiInt)
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusOK,
			gin.H{"luas: ": getLuas})
		c.Abort()
		return
	} else {
		getKeliling := kelilingPersegi(sisiInt)
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusOK,
			gin.H{"keliling: ": getKeliling})
		c.Abort()
		return
	}
}

func PersegiPanjang(c *gin.Context) {
	hitung := c.Query("hitung")
	panjang := c.Query("panjang")
	lebar := c.Query("lebar")
	panjangInt, _ := strconv.Atoi(panjang)
	lebarInt, _ := strconv.Atoi(lebar)
	var luasPersegiPanjang = func(p, l int) int{
		return p * l
	}

	var kelilingPersegiPanjang = func(p, l int) int{
		return 2 * (p + l)
	}

	if hitung == "luas" {
		getLuas := luasPersegiPanjang(panjangInt, lebarInt)
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusOK,
			gin.H{"luas: ": getLuas})
		c.Abort()
		return
	} else {
		getKeliling := kelilingPersegiPanjang(panjangInt, lebarInt)
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusOK,
			gin.H{"keliling: ": getKeliling})
		c.Abort()
		return
	}
}

func Lingkaran(c *gin.Context) {
	hitung := c.Query("hitung")
	jariJari := c.Query("jari-jari")
	jariJariFloat, _ := strconv.ParseFloat(jariJari, 64)
	var luasLingkaran = func(j float64) float64{
		return math.Pi * j * j
	}

	var kelilingLingkaran = func(j float64) float64{
		return math.Pi * (j + j)
	}

	if hitung == "luas" {
		getLuas := luasLingkaran(jariJariFloat)
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusOK,
			gin.H{"luas: ": getLuas})
		c.Abort()
		return
	} else {
		getKeliling := kelilingLingkaran(jariJariFloat)
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusOK,
			gin.H{"keliling: ": getKeliling})
		c.Abort()
		return
	}
}

func JajarGenjang(c *gin.Context) {
	hitung := c.Query("hitung")
	alas := c.Query("alas")
	tinggi := c.Query("tinggi")
	sisi := c.Query("sisi")
	alasInt, _ := strconv.Atoi(alas)
	tinggiInt, _ := strconv.Atoi(tinggi)
	sisiInt, _ := strconv.Atoi(sisi)

	var luasSegitigaSamaSisi = func(a, t int) int{
		return a * t
	}

	var kelilingJajarGenjang = func(a, s int) int{
		return (2 * a)+(2 * s)
	}

	if hitung == "luas" {
		getLuas := luasSegitigaSamaSisi(alasInt, tinggiInt)
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusOK,
			gin.H{"luas: ": getLuas})
		c.Abort()
		return
	} else {
		getKeliling := kelilingJajarGenjang(alasInt, sisiInt)
		c.Header("Content-Type", "application/json")
		c.JSON(http.StatusOK,
			gin.H{"keliling: ": getKeliling})
		c.Abort()
		return
	}
}