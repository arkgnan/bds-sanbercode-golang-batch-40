package book

import (
	"database/sql"
	"fmt"
	"net/http"
	"regexp"
	"time"

	"github.com/gin-gonic/gin"

	conn "restapi/dbsetup"
	"restapi/models"
)

// declare global variable
var db, err = conn.InitPostgres() 
var t = time.Now()
var formattedDateNow = fmt.Sprintf("%d-%02d-%02d %d:%02d:%02d", t.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), t.Second())
var book models.Book
var books []models.Book

func CreateBook(ctx *gin.Context) {	
	if err := ctx.ShouldBindJSON(&book); err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	} 
	var thickness string
	if book.TotalPage <= 100 {
		thickness = "tipis"
	} else if book.TotalPage > 100 && book.TotalPage <= 200 {
		thickness = "sedang"
	} else {
		thickness = "tebal"
	}

	var errorMessage []string
	var errorMessageYear string = ""
	var errorMessageUrl string = ""
	if book.ReleaseYear < 1980 {
		errorMessageYear = "Tahun rilis minimal adalah 1980"
		errorMessage = append(errorMessage, errorMessageYear)
	}
	var regex, _ = regexp.Compile(`(https?:\/\/.*\.(?:png|jpg))`)
	if !regex.MatchString(book.ImageUrl) {
		errorMessageUrl = "Image url tidak valid"
		errorMessage = append(errorMessage, errorMessageUrl)
	}


	if errorMessageUrl!="" || errorMessageYear!="" {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"error_status": "Data Not Found",
			"error_message": errorMessage,
		})
		return
	}

	sqlStatement := `
	INSERT INTO book (title, description, image_url, release_year, price, total_page, thickness, created_at, updated_at, category_id)
	VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)
	Returning *
	`
	err = db.QueryRow(sqlStatement, book.Title, book.Description, book.ImageUrl, book.ReleaseYear, book.Price, book.TotalPage, thickness, formattedDateNow, formattedDateNow, book.CategoryId).
		Scan(&book.Id, &book.Title, &book.Description, &book.ImageUrl, &book.ReleaseYear, &book.Price, &book.TotalPage, &book.Thickness, &book.CreatedAt, &book.UpdatedAt, &book.CategoryId)
	// defer db.Close()
	if err != nil {
		panic(err)
	}
	ctx.JSON(http.StatusCreated, gin.H {
		"data": book,
	})
}

func UpdateBook(ctx *gin.Context) {
	bookId := ctx.Param("id")

	if err := ctx.ShouldBindJSON(&book); err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	var thickness string
	if book.TotalPage <= 100 {
		thickness = "tipis"
	} else if book.TotalPage > 100 && book.TotalPage <= 200 {
		thickness = "sedang"
	} else {
		thickness = "tebal"
	}

	sqlStatement := `
	UPDATE book
	SET title = $2, description = $3, image_url = $4, release_year = $5, price = $6, total_page = $7, thickness = $8, updated_at = $9, category_id = $10
	WHERE id = $1;
	`
	res, err := db.Exec(sqlStatement, bookId, book.Title, book.Description, book.ImageUrl, book.ReleaseYear, book.Price, book.TotalPage, thickness, formattedDateNow, book.CategoryId)

	if err != nil {
		panic(err)
	}
	count, err := res.RowsAffected()
	if err != nil {
		panic(err)
	}

	if count==0 {
		ctx.AbortWithStatusJSON(http.StatusNotFound, gin.H{
			"error_status": "Data Not Found",
			"error_message": fmt.Sprintf("Book with id %v not found", bookId),
		})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"message": fmt.Sprintf("Book with id %v has been successfully updated", bookId),
	})
}

func GetBook(ctx *gin.Context) {
	// defer db.Close()
	bookId := ctx.Param("id")

	sqlStatement := `SELECT * from book WHERE id = $1`

	row := db.QueryRow(sqlStatement, bookId)

	switch err := row.Scan(&book.Id, &book.Title, &book.Description, &book.ImageUrl, &book.ReleaseYear, &book.Price, &book.TotalPage, &book.Thickness, &book.CreatedAt, &book.UpdatedAt, &book.CategoryId); err {
		case sql.ErrNoRows:
			ctx.AbortWithStatusJSON(http.StatusNotFound, gin.H{
				"error_status": "Data Not Found",
				"error_message": fmt.Sprintf("Book with id %v not found", bookId),
			})
			return
		case nil:
			ctx.JSON(http.StatusOK, gin.H{
				"data": book,
			})
		default:
			panic(err)
	}
}

func DeleteBook(ctx *gin.Context) {
	bookId := ctx.Param("id")

	if err := ctx.ShouldBindJSON(&book); err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	sqlStatement := `
	DELETE from book
	WHERE id = $1;
	`
	res, err := db.Exec(sqlStatement, bookId)
	if err != nil {
		panic(err)
	}
	count, err := res.RowsAffected()
	if err != nil {
		panic(err)
	}

	if count==0 {
		ctx.AbortWithStatusJSON(http.StatusNotFound, gin.H{
			"error_status": "Data Not Found",
			"error_message": fmt.Sprintf("Book with id %v not found", bookId),
		})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"message": fmt.Sprintf("Book with id %v has been successfully deleted", bookId),
	})
}

func GetAllBook(ctx *gin.Context) {
	sqlStatement := `SELECT * from book`

	rows, err := db.Query(sqlStatement)
	if err != nil {
		panic(err)
	}
	for rows.Next() {
		err = rows.Scan(&book.Id, &book.Title, &book.Description, &book.ImageUrl, &book.ReleaseYear, &book.Price, &book.TotalPage, &book.Thickness, &book.CreatedAt, &book.UpdatedAt, &book.CategoryId)

		if err != nil {
			panic(err)
		}

		books = append(books, book)
	}
	ctx.JSON(http.StatusOK, gin.H{
		"data": books,
	})
}

