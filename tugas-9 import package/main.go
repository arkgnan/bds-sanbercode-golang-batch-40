package main

import (
	"fmt"
	"tugas-9/helpers"
)

func main(){
	// soal 1
	var bangunDatar helpers.HitungBangunDatar
	bangunDatar = helpers.SegitigaSamaSisi{8, 4}
	fmt.Println("===== segitiga sama sisi")
	fmt.Println(bangunDatar.Luas())
	fmt.Println(bangunDatar.Keliling())

	bangunDatar = helpers.PersegiPanjang{8, 4}
	fmt.Println("===== persegi panjang")
	fmt.Println(bangunDatar.Luas())
	fmt.Println(bangunDatar.Keliling())

	var bangunRuang helpers.HitungBangunRuang
	bangunRuang = helpers.Tabung{8, 4}
	fmt.Println("===== tabung")
	fmt.Println(bangunRuang.Volume())
	fmt.Println(bangunRuang.LuasPermukaan())

	bangunRuang = helpers.Balok{8, 4, 4}
	fmt.Println("===== balok")
	fmt.Println(bangunRuang.Volume())
	fmt.Println(bangunRuang.LuasPermukaan())

	// soal 2
	var hape helpers.SpecPonsel
	hape = helpers.Phone{"S7", "Samsung", 2016, []string{"White", "Black"}}
	hape.CetakSpec()

	// soal 3
	fmt.Println(helpers.LuasPersegi(4, true))

	fmt.Println(helpers.LuasPersegi(8, false))

	fmt.Println(helpers.LuasPersegi(0, true))

	fmt.Println(helpers.LuasPersegi(0, false))


	// soal 4
	helpers.SoalKeEmpat()
}