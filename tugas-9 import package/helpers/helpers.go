package helpers

import (
	"fmt"
	"math"
	"strconv"
	"strings"
)

type SegitigaSamaSisi struct{
	Alas, Tinggi int
}
  
type PersegiPanjang struct{
	Panjang, Lebar int
}
  
type Tabung struct{
	JariJari, Tinggi float64
}
  
type Balok struct{
	Panjang, Lebar, Tinggi int
}
  
type HitungBangunDatar interface{
	Luas() int
	Keliling() int
}
  
type HitungBangunRuang interface{
	Volume() float64
	LuasPermukaan() float64
}

func (s SegitigaSamaSisi) Luas() int {
	var luas float64 = 0.5 * float64(s.Alas) * float64(s.Tinggi)
	return int(math.Round(luas))
}
  
func (s SegitigaSamaSisi) Keliling() int {
	return s.Alas + s.Alas + s.Alas
}

func (p PersegiPanjang) Luas() int {
	return p.Panjang * p.Lebar
}
  
func (p PersegiPanjang) Keliling() int {
	return (2*p.Panjang)+(2*p.Lebar)
}

func (t Tabung) Volume() float64 {
	// π x r² x t 
	return math.Pi * math.Pow(t.JariJari, 2) * t.Tinggi
}

func (b Balok) Volume() float64 {
	return float64(b.Panjang) * float64(b.Lebar) * float64(b.Tinggi)
}

func (t Tabung) LuasPermukaan() float64 {
	// 2 x π x r x (r + t)
	return 2 * math.Pi * t.JariJari * (t.JariJari + t.Tinggi)
}

func (b Balok) LuasPermukaan() float64 {
	// pl + 2(lt) + 2(pt)
	return (float64(b.Panjang)*float64(b.Lebar))+2*(float64(b.Lebar)*float64(b.Tinggi))+2*(float64(b.Panjang)*float64(b.Tinggi))
}

type Phone struct{
	Name, Brand string
	Year int
	Colors []string
}

func (p Phone) CetakSpec() {
	fmt.Println("name : ",p.Name)
	fmt.Println("brand : ",p.Brand)
	fmt.Println("year : ",p.Year)
	colorString := strings.Join(p.Colors, ",")
	fmt.Println("colors : ",colorString)
}

type SpecPonsel interface{
	CetakSpec()
}

func LuasPersegi(angka int, status bool) interface{}{
	if status {
		if angka==0 {
			return "Maaf anda belum menginput sisi dari persegi"
		} else {
			return "luas persegi dengan sisi 2 cm adalah 4 cm"
		}
	} else {
		if angka==0 {
			return nil
		} else {
			return angka
		}
	}
}

func SoalKeEmpat(){

	// soal 4
	var prefix interface{}= "hasil penjumlahan dari "

	var kumpulanAngkaPertama interface{} = []int{6,8}

	var kumpulanAngkaKedua interface{} = []int{12,14}

	var totalAngkaPertama int
	var stringAngkaPertama string
	for i, angkaPertama := range kumpulanAngkaPertama.([]int) {
		totalAngkaPertama += angkaPertama
		if i==0 {
			stringAngkaPertama = strconv.Itoa(angkaPertama)
		} else {
			stringAngkaPertama += " + "+strconv.Itoa(angkaPertama)
		}
	}
	var totalAngkaKedua int
	var stringAngkaKedua string
	for i, angkaKedua := range kumpulanAngkaKedua.([]int) {
		totalAngkaKedua += angkaKedua
		if i==0 {
			stringAngkaKedua = strconv.Itoa(angkaKedua)
		} else {
			stringAngkaKedua += " + "+strconv.Itoa(angkaKedua)
		}
	}
	total := totalAngkaPertama + totalAngkaKedua
	fmt.Println(prefix.(string), stringAngkaPertama, "+" , stringAngkaKedua, "=", total)
}