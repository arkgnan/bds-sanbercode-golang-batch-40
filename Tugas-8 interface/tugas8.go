package main

import (
	"fmt"
	"math"
	"strconv"
	"strings"
)

type segitigaSamaSisi struct{
	alas, tinggi int
}
  
type persegiPanjang struct{
	panjang, lebar int
}
  
type tabung struct{
	jariJari, tinggi float64
}
  
type balok struct{
	panjang, lebar, tinggi int
}
  
type hitungBangunDatar interface{
	luas() int
	keliling() int
}
  
type hitungBangunRuang interface{
	volume() float64
	luasPermukaan() float64
}

func (s segitigaSamaSisi) luas() int {
	var luas float64 = 0.5 * float64(s.alas) * float64(s.tinggi)
	return int(math.Round(luas))
}
  
func (s segitigaSamaSisi) keliling() int {
	return s.alas + s.alas + s.alas
}

func (p persegiPanjang) luas() int {
	return p.panjang * p.lebar
}
  
func (p persegiPanjang) keliling() int {
	return (2*p.panjang)+(2*p.lebar)
}

func (t tabung) volume() float64 {
	// π x r² x t 
	return math.Pi * math.Pow(t.jariJari, 2) * t.tinggi
}

func (b balok) volume() float64 {
	return float64(b.panjang) * float64(b.lebar) * float64(b.tinggi)
}

func (t tabung) luasPermukaan() float64 {
	// 2 x π x r x (r + t)
	return 2 * math.Pi * t.jariJari * (t.jariJari + t.tinggi)
}

func (b balok) luasPermukaan() float64 {
	// pl + 2(lt) + 2(pt)
	return (float64(b.panjang)*float64(b.lebar))+2*(float64(b.lebar)*float64(b.tinggi))+2*(float64(b.panjang)*float64(b.tinggi))
}

type phone struct{
	name, brand string
	year int
	colors []string
}

func (p phone) cetakSpec() {
	fmt.Println("name : ",p.name)
	fmt.Println("brand : ",p.brand)
	fmt.Println("year : ",p.year)
	colorString := strings.Join(p.colors, ",")
	fmt.Println("colors : ",colorString)
}

type specPonsel interface{
	cetakSpec()
}

func main(){
	// soal 1
	var bangunDatar hitungBangunDatar
	bangunDatar = segitigaSamaSisi{8, 4}
	fmt.Println("===== segitiga sama sisi")
	fmt.Println(bangunDatar.luas())
	fmt.Println(bangunDatar.keliling())

	bangunDatar = persegiPanjang{8, 4}
	fmt.Println("===== persegi panjang")
	fmt.Println(bangunDatar.luas())
	fmt.Println(bangunDatar.keliling())

	var bangunRuang hitungBangunRuang
	bangunRuang = tabung{8, 4}
	fmt.Println("===== tabung")
	fmt.Println(bangunRuang.volume())
	fmt.Println(bangunRuang.luasPermukaan())

	bangunRuang = balok{8, 4, 4}
	fmt.Println("===== balok")
	fmt.Println(bangunRuang.volume())
	fmt.Println(bangunRuang.luasPermukaan())

	// soal 2
	var hape specPonsel
	hape = phone{"S7", "Samsung", 2016, []string{"White", "Black"}}
	hape.cetakSpec()

	// soal 3
	var luasPersegi = func (angka int, status bool) interface{}{
		if status {
			if angka==0 {
				return "Maaf anda belum menginput sisi dari persegi"
			} else {
				return "luas persegi dengan sisi 2 cm adalah 4 cm"
			}
		} else {
			if angka==0 {
				return nil
			} else {
				return angka
			}
		}
	}

	fmt.Println(luasPersegi(4, true))

	fmt.Println(luasPersegi(8, false))

	fmt.Println(luasPersegi(0, true))

	fmt.Println(luasPersegi(0, false))



	// soal 4
	var prefix interface{}= "hasil penjumlahan dari "

	var kumpulanAngkaPertama interface{} = []int{6,8}

	var kumpulanAngkaKedua interface{} = []int{12,14}

	var totalAngkaPertama int
	var stringAngkaPertama string
	for i, angkaPertama := range kumpulanAngkaPertama.([]int) {
		totalAngkaPertama += angkaPertama
		if i==0 {
			stringAngkaPertama = strconv.Itoa(angkaPertama)
		} else {
			stringAngkaPertama += " + "+strconv.Itoa(angkaPertama)
		}
	}
	var totalAngkaKedua int
	var stringAngkaKedua string
	for i, angkaKedua := range kumpulanAngkaKedua.([]int) {
		totalAngkaKedua += angkaKedua
		if i==0 {
			stringAngkaKedua = strconv.Itoa(angkaKedua)
		} else {
			stringAngkaKedua += " + "+strconv.Itoa(angkaKedua)
		}
	}
	total := totalAngkaPertama + totalAngkaKedua
	fmt.Println(prefix.(string), stringAngkaPertama, "+" , stringAngkaKedua, "=", total)
}