package main

import (
	"fmt"
)

func main(){	
	// soal 1
	var luasLigkaran float64 
	var kelilingLingkaran float64

	fmt.Println("luas :", luasLigkaran)
	fmt.Println("keliling :", kelilingLingkaran)

	calculateLuasLigkaran(&luasLigkaran, 5)
	calculateKelilingLingkaran(&kelilingLingkaran, 10)

	fmt.Println("luas :", luasLigkaran)
	fmt.Println("keliling :", kelilingLingkaran)

	// soal 2
	var sentence string 
	introduce(&sentence, "John", "laki-laki", "penulis", "30")

	fmt.Println(sentence) // Menampilkan "Pak John adalah seorang penulis yang berusia 30 tahun"
	introduce(&sentence, "Sarah", "perempuan", "model", "28")

	fmt.Println(sentence) // Menampilkan "Bu Sarah adalah seorang model yang berusia 28 tahun"

	// soal 3
	var buah = []string{}
	buahFavorit(&buah, "Jeruk", "Semangka", "Mangga", "Strawberry", "Durian", "Manggis", "Alpukat")
	for i, bu := range buah {
		no := i+1;
		fmt.Printf("%d. %s\n", no, bu)
	}

	// soal 4
	var dataFilm = []map[string]string{}
	tambahDataFilm("LOTR", "2 jam", "action", "1999", &dataFilm)
	tambahDataFilm("avenger", "2 jam", "action", "2019", &dataFilm)
	tambahDataFilm("spiderman", "2 jam", "action", "2004", &dataFilm)
	tambahDataFilm("juon", "2 jam", "horror", "2004", &dataFilm)

	for i, film := range dataFilm {
		no := i+1;
		fmt.Println(no, "Genre:", film["genre"])
		fmt.Println("  Year:", film["tahun"])
		fmt.Println("  Title:", film["title"])
		fmt.Println("  Duration:", film["jam"])
		fmt.Println("  ")
	}

}

func calculateLuasLigkaran(luas *float64, jarijari float64) {
	*luas = 3.14 * jarijari * jarijari
}

func calculateKelilingLingkaran(lingkaran *float64, diameter float64) {
	*lingkaran = 3.14 * diameter
}

func introduce(kalimat *string, name string, gender string, job string, age string){
	var sapaan string
	if gender == "laki-laki" {
		sapaan = "Pak"
	} else {
		sapaan = "Bu"
	}
	*kalimat = sapaan+" "+name+" adalah seorang "+job+" yang berusia "+age+" tahun"
}

func buahFavorit(fruits *[]string, allfruit ...string) *[]string{
	for _, fruit := range allfruit {
		*fruits = append(*fruits, fruit)
	}
	return fruits
}

func tambahDataFilm(title, duration, genre, releaseYear string, dataFilm *[]map[string]string){
	var film = map[string]string{
		"genre": genre,
		"jam": duration,
		"tahun": releaseYear,
		"title": title,
	}
	*dataFilm = append(*dataFilm, film)
}