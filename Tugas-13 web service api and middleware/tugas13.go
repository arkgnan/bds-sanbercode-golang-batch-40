package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"strconv"
)

func Auth(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		uname, pwd, ok := r.BasicAuth()
		if !ok {
			w.Write([]byte("Username atau Password tidak boleh kosong"))
			return
		}
	
		if uname == "admin" && pwd == "password" {
			next.ServeHTTP(w, r)
			return
		}
		w.Write([]byte("Username atau Password tidak sesuai"))
		return
	})
}

type NilaiMahasiswa struct{
	Nama string `json:"nama"`
	MataKuliah string `json:"mata_kuliah"`
	IndeksNilai string 
	Nilai, ID uint
}
  
var nilaiNilaiMahasiswa = []NilaiMahasiswa{}

  
func storeNilai(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		// soal 2
		dataNilaiMahasiswa, err := json.Marshal(nilaiNilaiMahasiswa)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(dataNilaiMahasiswa)
	} else if r.Method == "POST" {
		// soal 1
		var err error
		var nilaiMahasiswa = NilaiMahasiswa{}

		if r.Header.Get("Content-Type") == "application/json" {
			decodeJSON := json.NewDecoder(r.Body)
			if err := decodeJSON.Decode(&nilaiMahasiswa); err != nil {
				log.Fatal(err)
			}
			nilaiMahasiswa.IndeksNilai, err = calculateNilai(int(nilaiMahasiswa.Nilai))
			if err != nil{
				w.Write([]byte(err.Error())) 
				return
			}
			nilaiMahasiswa.ID = generateId(nilaiMahasiswa.Nama, &nilaiNilaiMahasiswa)
		} else {
			getNilai := r.PostFormValue("nilai")
			nilai, _ := strconv.Atoi(getNilai)
			nama := r.PostFormValue("nama")
			mataKuliah := r.PostFormValue("mata_kuliah")

			nilaiMahasiswa.Nama  = nama
			nilaiMahasiswa.MataKuliah  = mataKuliah
			nilaiMahasiswa.Nilai  = uint(nilai)

			nilaiMahasiswa.IndeksNilai, err = calculateNilai(nilai)
			if err != nil{
				w.Write([]byte(err.Error())) 
				return
			}
			nilaiMahasiswa.ID = generateId(nama, &nilaiNilaiMahasiswa)
		}
		nilaiNilaiMahasiswa = append(nilaiNilaiMahasiswa, nilaiMahasiswa)
		dataMahasiswa, _ := json.Marshal(nilaiMahasiswa)
    	w.Write(dataMahasiswa) 
		return
	} else {
		http.Error(w, "METHOD NOT ALLOWED", http.StatusBadRequest)
	}
}

func calculateNilai(nilai int) (string, error) {
	if(nilai > 100){
		return "", errors.New("Nilai tidak boleh lebih dari 100")
	} else {
		if nilai >= 80 {
			return "A", nil;
		} else if nilai >= 70 && nilai<80{
			return "B", nil;
		} else if nilai >= 60 && nilai<70{
			return "C", nil;
		} else if nilai >= 50 && nilai<60{
			return "C", nil;
		} else {
			return "E", nil;
		}
	}
}

func generateId(nama string, totalData *[]NilaiMahasiswa) uint {
	for k, v := range *totalData {
        if nama == v.Nama {
			posisi := k+1
            return uint(posisi)
        }
    }
	// random number for new data
	return uint(rand.Uint64())
}


func main() {
	// konfigurasi server
	server := &http.Server{
		Addr: ":8080",
	}

	// routing
	http.Handle("/", Auth(http.HandlerFunc(storeNilai)))

	// jalankan server
	fmt.Println("server running at http://localhost:8080")
	server.ListenAndServe()
}