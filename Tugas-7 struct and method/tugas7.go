package main

import (
	"fmt"
)

type buah struct {
    nama, warna string
	adaBijinya bool
	harga int
}

type segitiga struct{
	alas, tinggi int
}

type persegi struct{
	sisi int
}

type persegiPanjang struct{
	panjang, lebar int
}

type phone struct{
	name, brand string
	year int
	colors []string
}

type movie struct{
	title, genre string
	duration, year int
}

func main(){
	// soal 1
	nanas := buah{"Nanas", "Kuning", false, 9000}
	jeruk := buah{"Jeruk", "Oranye", true, 8000}
	semangka := buah{"Semangka", "Hijau & Merah", true, 10000}
	pisang := buah{"Pisang", "Kuning", false, 5000}
	buahbuahan := []buah{nanas, jeruk, semangka, pisang}
	fmt.Println(buahbuahan)

	// soal 2
	segitiga := segitiga{4, 2}
	segitiga.luasSegitiga()

	persegi := persegi{4}
	persegi.kelilingPersegi()

	persegiPanjang := persegiPanjang{4, 2}
	persegiPanjang.luasPersegiPanjang()

	// soal 3
	hape := phone{}
	hape.name = "S7"
	hape.brand = "Samsung"
	hape.year = 2016
	specColor(&hape.colors, "Black")
	specColor(&hape.colors, "White")
	fmt.Println(hape)

	// soal 4
	var dataFilm = []movie{}

	tambahDataFilm("LOTR", 120, "action", 1999, &dataFilm)
	tambahDataFilm("avenger", 120, "action", 2019, &dataFilm)
	tambahDataFilm("spiderman", 120, "action", 2004, &dataFilm)
	tambahDataFilm("juon", 120, "horror", 2004, &dataFilm)

	for i, film := range dataFilm {
		no := i+1;
		fmt.Println(no, "Genre:", film.genre)
		fmt.Println("  Year:", film.year)
		fmt.Println("  Title:", film.title)
		fmt.Println("  Duration:", film.duration)
		fmt.Println("  ")
	}
}

func (s segitiga) luasSegitiga(){
	alasSegitigaFloat := float32(s.alas)
	tinggiSegitigaFloat := float32(s.tinggi)
	luasSegitigaFloat := 1/2.0 * (alasSegitigaFloat * tinggiSegitigaFloat)
	var luasSegitiga int = int(luasSegitigaFloat)
	fmt.Println(luasSegitiga)
}

func (s persegi) kelilingPersegi(){
	var kelilingPersegi int = 4 * s.sisi
	fmt.Println(kelilingPersegi)
}

func (s persegiPanjang) luasPersegiPanjang(){
	var luasPersegi int = s.panjang * s.lebar
	fmt.Println(luasPersegi)
}

func specColor(spec *[]string, color string){
	*spec = append(*spec, color)
}

func tambahDataFilm(title string, duration int, genre string, releaseYear int, dataFilm *[]movie){
	film := movie{}
	film.title = title
	film.genre = genre
	film.duration = duration
	film.year = releaseYear
	
	*dataFilm = append(*dataFilm, film)
}