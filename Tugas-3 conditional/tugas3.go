package main

import (
	"fmt"
	"strconv"
)

func main()  {
	// soal 1
	var panjangPersegiPanjang string = "8"
	var lebarPersegiPanjang string = "5"

	var alasSegitiga string = "6"
	var tinggiSegitiga string = "7"

	panjangPersegiPanjangInt, _ := strconv.Atoi(panjangPersegiPanjang)
	lebarPersegiPanjangInt, _ := strconv.Atoi(lebarPersegiPanjang)
	alasSegitigaInt, _ := strconv.ParseFloat(alasSegitiga, 32)
	tinggiSegitigaInt, _ := strconv.ParseFloat(tinggiSegitiga, 32)

	var luasPersegiPanjang int = panjangPersegiPanjangInt * lebarPersegiPanjangInt
	var kelilingPersegiPanjang int = 2*(panjangPersegiPanjangInt+lebarPersegiPanjangInt)
	luasSegitigaFloat := 1/2.0 * (alasSegitigaInt * tinggiSegitigaInt)
	var luasSegitiga int = int(luasSegitigaFloat)

	fmt.Printf("Luas Persegi Panjang %d\n", luasPersegiPanjang)
	fmt.Printf("Keliling Persegi Panjang %d\n", kelilingPersegiPanjang)
	fmt.Printf("Luas segitiga %d\n", luasSegitiga)


	// soal 2	
	if nilaiJohn := 80; nilaiJohn >= 80 {
		fmt.Println("Index John : A")
	} else if nilaiJohn >= 70 && nilaiJohn<80{
		fmt.Println("Index John : B")
	} else if nilaiJohn >= 60 && nilaiJohn<70{
		fmt.Println("Index John : C")
	} else if nilaiJohn >= 50 && nilaiJohn<60{
		fmt.Println("Index John : D")
	} else {
		fmt.Println("Index John : E")
	}

	if nilaiDoe := 50; nilaiDoe >= 80 {
		fmt.Println("Index Doe : A")
	} else if nilaiDoe >= 70 && nilaiDoe<80{
		fmt.Println("Index Doe : B")
	} else if nilaiDoe >= 60 && nilaiDoe<70{
		fmt.Println("Index Doe : C")
	} else if nilaiDoe >= 50 && nilaiDoe<60{
		fmt.Println("Index Doe : D")
	} else {
		fmt.Println("Index Doe : E")
	}

	// soal 3		
	var tanggal = 29;
	var bulan = 12; 
	var tahun = 1990;

	switch bulan {
		case 1:
			bulan := "Januari"
			fmt.Println(tanggal, bulan, tahun)
		case 2:
			bulan := "Februari"
			fmt.Println(tanggal, bulan, tahun)
		case 3:
			bulan := "Maret"
			fmt.Println(tanggal, bulan, tahun)
		case 4:
			bulan := "April"
			fmt.Println(tanggal, bulan, tahun)
		case 5:
			bulan := "Mei"
			fmt.Println(tanggal, bulan, tahun)
		case 6:
			bulan := "Juni"
			fmt.Println(tanggal, bulan, tahun)
		case 7:
			bulan := "Juli"
			fmt.Println(tanggal, bulan, tahun)
		case 8:
			bulan := "Agustus"
			fmt.Println(tanggal, bulan, tahun)
		case 9:
			bulan := "September"
			fmt.Println(tanggal, bulan, tahun)
		case 10:
			bulan := "Oktober"
			fmt.Println(tanggal, bulan, tahun)
		case 11:
			bulan := "November"
			fmt.Println(tanggal, bulan, tahun)
		case 12:
			bulan := "Desember"
			fmt.Println(tanggal, bulan, tahun)
	}

	// soal 4
	if tahun>=1944 && tahun<=1964 {
		fmt.Println("Baby Booomer")
	} else if tahun>=1965 && tahun<=1979 {
		fmt.Println("Generasi X")
	} else if tahun>=1980 && tahun<=1994 {
		fmt.Println("Generasi Y (Millenials)")
	} else if tahun>=1995 && tahun<=2015 {
		fmt.Println("Generasi Z")
	}
	

}