package main

import (
	"fmt"
	"sort"
	"sync"
	"time"
)


func urutkan(list []string, wg *sync.WaitGroup){
	sort.Strings(list)
	for i, urut := range list{
		time.Sleep(time.Second)
		fmt.Println(i+1, urut)
	}
	wg.Done()
}

func getMovies(ch chan string, films ...string){
	for _, film := range films{
		ch <- film
	}
	close(ch)
}

func main()  {
	var wg sync.WaitGroup
	
	// soal 1
	var phones = []string{"Xiaomi", "Asus", "Iphone", "Samsung", "Oppo", "Realme", "Vivo"}
	wg.Add(1)
	go urutkan(phones, &wg)

	wg.Wait()

	// soal 2
	var movies = []string{"Harry Potter", "LOTR", "SpiderMan", "Logan", "Avengers", "Insidious", "Toy Story"}

	moviesChannel := make(chan string)

	go getMovies(moviesChannel, movies...)

	for value := range moviesChannel {
		fmt.Println(value)
	}
}